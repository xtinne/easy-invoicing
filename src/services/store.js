import Vuex from 'vuex';
import Vue from 'vue';
import { vuexfireMutations, firestoreAction } from 'vuexfire';


Vue.use(Vuex);

const store = new Vuex.Store({
  debug: true,
  state: {
    categories: [],
    suppliers: [],
    invoicesAntwerpen: [],
    invoicesDeurne: [],
    user: {
      loggedIn: false,
      data: null,
    },
  },
  getters: {
    user(state) {
      return state.user;
    },
  },
  mutations: {
    SET_LOGGED_IN(state, value) {
      state.user.loggedIn = value;
    },
    SET_USER(state, data) {
      state.user.data = data;
    },
    ...vuexfireMutations,
  },
  actions: {
    setCategories: firestoreAction((context, ref) => {
      context.bindFirestoreRef('categories', ref);
    }),
    setSuppliers: firestoreAction((context, ref) => {
      context.bindFirestoreRef('suppliers', ref);
    }),
    setInvoicesAntwerpen: firestoreAction((context, ref) => {
      context.bindFirestoreRef('invoicesAntwerpen', ref);
    }),
    setInvoicesDeurne: firestoreAction((context, ref) => {
      context.bindFirestoreRef('invoicesDeurne', ref);
    }),
    fetchUser({ commit }, user) {
      commit('SET_LOGGED_IN', user !== null);
      if (user) {
        commit('SET_USER', {
          displayName: user.displayName,
          email: user.email,
        });
      } else {
        commit('SET_USER', null);
      }
    },
  },
});

export default store;
