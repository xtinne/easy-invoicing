import firebase from 'firebase/app';
import 'firebase/firestore';
import 'firebase/auth';

import store from './store';

const configOptions = {
  apiKey: 'SECRET_KEY' ,
  authDomain: 'AUTH_DOMAIN',
  databaseURL: 'DB_URL',
  projectId: 'PROJECT_ID',
  storageBucket: '',
  messagingSenderId: '',
  appId: '',
};

// Get a Firestore instance
export const db = firebase
  .initializeApp(configOptions)
  .firestore();

firebase.auth().onAuthStateChanged((user) => {
  store.dispatch('fetchUser', user);
});

// Export types that exists in Firestore
// This is not always necessary, but it's used in other examples
const { Timestamp, GeoPoint } = firebase.firestore;
export { Timestamp, GeoPoint };
