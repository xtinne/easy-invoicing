import Vue from 'vue';
import VueRouter from 'vue-router';
import InvoicesList from '../components/InvoicesList.vue';
import NewCategory from '../components/NewCategory.vue';
import NewSupplier from '../components/NewSupplier.vue';
import NewInvoice from '../components/NewInvoice.vue';
import Suppliers from '../components/Suppliers.vue';
import Categories from '../components/Categories.vue';
import Home from '../components/Home.vue';
import InvoicesOverview from '../components/InvoicesOverview.vue';
import YearSelection from '../components/YearSelection.vue';
import EditInvoice from '../components/EditInvoice.vue';
import Login from '../components/Login.vue';

Vue.use(VueRouter);

const routes = [
  {
    path: '/',
    name: 'Home',
    component: Home,
    meta: { requiresAuth: true },
  },
  {
    path: '/login',
    name: 'Login',
    component: Login,
  },
  {
    path: '/years/:campus',
    name: 'YearSelection',
    component: YearSelection,
    meta: { requiresAuth: true },
  },
  {
    path: '/invoices/:campus/:year/:categoryId?',
    name: 'InvoicesList',
    component: InvoicesList,
    meta: { requiresAuth: true },
  },
  {
    path: '/overview/:campus/:year',
    name: 'InvoicesOverview',
    component: InvoicesOverview,
    meta: { requiresAuth: true },
  },
  {
    path: '/new-category/:campus/:year',
    name: 'NewCategory',
    component: NewCategory,
    meta: { requiresAuth: true },
  },
  {
    path: '/new-supplier/:campus/:year',
    name: 'NewSupplier',
    component: NewSupplier,
    meta: { requiresAuth: true },
  },
  {
    path: '/new-invoice/:campus/:year/:categoryId?',
    name: 'NewInvoice',
    component: NewInvoice,
    meta: { requiresAuth: true },
  },
  {
    path: '/edit-invoice/:campus/:year/:invoiceId?',
    name: 'EditInvoice',
    component: EditInvoice,
    meta: { requiresAuth: true },
  },
  {
    path: '/suppliers/:campus/:year',
    name: 'SuppliersList',
    component: Suppliers,
    meta: { requiresAuth: true },
  },
  {
    path: '/categories/:campus/:year',
    name: 'CategoriesList',
    component: Categories,
    meta: { requiresAuth: true },
  },
  { path: '*', component: Home },
];

const router = new VueRouter({
  routes,
  mode: 'history',
});

export default router;
