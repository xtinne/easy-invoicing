import Vue from 'vue';
import SelectButton from 'primevue/selectbutton';
import DataTable from 'primevue/datatable';
import Column from 'primevue/column';
import ColumnGroup from 'primevue/columngroup';
import InputText from 'primevue/inputtext';
import Button from 'primevue/button';
import { firestorePlugin } from 'vuefire';
import ToastService from 'primevue/toastservice';
import Dropdown from 'primevue/dropdown';
import Toast from 'primevue/toast';
import ProgressSpinner from 'primevue/progressspinner';
import firebase from 'firebase/app';
import router from './router';
import { db } from './services/db';
import App from './App.vue';
import store from './services/store';

import 'firebase/auth';
import 'primevue/resources/themes/nova-light/theme.css';
import 'primevue/resources/primevue.min.css';
import 'primeicons/primeicons.css';


Vue.use(firestorePlugin);
Vue.use(ToastService);

Vue.config.productionTip = false;

Vue.component('SelectButton', SelectButton);
Vue.component('DataTable', DataTable);
Vue.component('Column', Column);
Vue.component('ColumnGroup', ColumnGroup);
Vue.component('InputText', InputText);
Vue.component('Button', Button);
Vue.component('Toast', Toast);
Vue.component('Dropdown', Dropdown);
Vue.component('ProgressSpinner', ProgressSpinner);

router.beforeEach((to, from, next) => {
  const { currentUser } = firebase.auth();
  // if (!currentUser) {
  //   currentUser = this.$store.state.loggedIn;
  // }
  const requiresAuth = to.matched.some((record) => record.meta.requiresAuth);

  if (requiresAuth && !currentUser) {
    const loginpath = window.location.pathname;
    next({ name: 'Login', query: { from: loginpath } });
  } else next();
});

new Vue({
  el: '#app',
  router,
  store,
  created() {
    this.$store.dispatch('setCategories', db.collection('categories'));
    this.$store.dispatch('setSuppliers', db.collection('suppliers'));
    this.$store.dispatch('setInvoicesAntwerpen', db.collection('invoices').where('campus', '==', 'antwerpen'));
    this.$store.dispatch('setInvoicesDeurne', db.collection('invoices').where('campus', '==', 'deurne'));
  },
  render: (h) => h(App),
}).$mount('#app');
